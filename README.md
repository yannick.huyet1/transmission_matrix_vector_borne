# Modeling the spread of a disease using a metapopulation model.





# Description

This folder contains three scripts, one main and two other examples. Each of them uses the same metapopulation model to simulate the spread of a vectorial disease.

## Background
In Senegal, the occurrence of Rift Valley Fever (RVF) outbreaks depends on the dynamics of the main vectors during the rainy season, and disease spread is influenced by animal mobility patterns, which in turn depend on resource availability and religious festivities such as Tabaski. We developed a mathematical model based on the concept of metapopulation to estimate the risk of RVF diffusion in Senegal. The model incorporates disease transmission through insect bites and animal contacts, as well as spread through animal mobility. 

## Metapopulation Model
We developed a mathematical model based on the concept of metapopulation to assess the risk of disease spread. It is a metapopulation model describing subpopulations of different species, spatially structured in areas called "patches" or sites. That represent nodes in a network.Inside a site there is local transmission of the disease between susceptibles animals and vectors. The sites are interconnected by animal movements.The links represent movements between different site and the weight of the link quantifies the number of individuals moving from one site to another.This approach effectively combines transmission dynamics within patches with diffusion dynamics related to animal mobility (see figure 1)

![Figure 1](figure_1.jpg) 

Considering the spatial scale of the metapopulation, mosquito mobility is not taken into account as it is relatively limited. Species such as Aedes aegypti typically fly distances ranging from 50 to 100 meters from their breeding sites, with some individuals capable of flying up to a maximum of 400 meters under optimal conditions. Therefore, it is the mobility of infected susceptible animals that can lead to the spread of a disease through the network.

animals have a probability p of moving from their site i, or staying there with a probability (1 - p). If they leave their site i, they move to a different subpopulation chosen from those connected to i. The choice is determined by the matrix R_ij. This matrix R_ij contains entries that account for the probability that a trip starting from site i will have site j as its destination. The R matrix can be calculated from the observed number of trips between each pair of sites (i, j) and normalized based on the total number of outgoing movements from site i.

Inside a site, we use a compartimental model for the local transmission of the disease (see figure 2). The vectors can be in two states: either susceptible (S) to develop the disease or infected (I). Currently, we consider that the size of the vector population remains constant during the period of time chosen within a site.The ruminant hosts can be in three states: either susceptible (S) to develop the disease, or infected (I), or recovered (R). The compartment R takes into account the animals that have become immune after recovering from the infection (with a recovery rate denoted as µ_H).

![Figure 2](figure_2.jpg)

Inour model, the probability λ of infection between mosquitoes and ruminants is not differentiated. In other words, the same value is used when a healthy mosquito gets infected during a blood meal on an infected ruminant (λ_HM) or when an infected mosquito feeds on a healthy ruminant (λ_MH). Concretely, λ is calculated as the average of λ_HM and λ_MH.
We consider that the size of the vector population remains constant within a site each, so we assume that the birth rate of vectors is the same as the mortality rate µ_M. It is also assumed that each vector feeds or "bites" ruminants at a certain biting rate β.


## Features

The scripts provided in this repository allow to identify the potential departments (nodes) who participated the most at the dynamic of transmission of the disease (through direct contamination between mosquitoes and animals hosts and also through animal mobility). 

To quantify indirect infections between ruminants mediated by vectors and vice versa, we compute this systeme of matrix equations :

![Figure 3](figure_3.jpg)

Epsilon H and Epsilon M are, respectively, the prevalence of the disease in susceptible animal hosts and vector hosts. M and M ̃tilde are the matrices described in   figure 4.

![Figure 4](figure_4.jpg)

Theses two matrices encompass all possible contagion processes between ruminants through vectors. There are four possible infection paths: (i) an infected ruminant from site i infects a vector from site i, which then infects another ruminant in site i; (ii) an infected ruminant from site j moves to site i and infects a vector that will subsequently transmit the disease to a ruminant residing in site i; (iii) a resident ruminant in site j infects a vector, and a susceptible ruminant moving from site j to i becomes infected there; (iv) both the infected ruminant from site j and the susceptible ruminant from site i move to a third contiguous patch k, where infection occurs through a vector (see figure 5). 

![Figure 5](figure_5.jpg)

"The non-trivial solutions, i.e., different from zero, for this system of equations (figure 3) correspond to the eigenvectors of the matrix MMtilde.

It's also possible to identify the departments that could be impacted by a disease. To do this, we calculate the value of the epidemic risk (ER) for each department.
![Figure 6](figure_6.jpg)

It is possible to generate  maps with normalised values of this risk for a given country. We will illustrate it with mock informations in the case of RVF in Senegal.

## Content of the repository

The repository contains 4 folders:
  
  * **Data**  containing:
    - [aedes_vexans.xlsx](data/aedes_vexans.xlsx), a mock example of an excel sheet containing the name of some departments in Senegal, the number of Number of female mosquitoes in search of a blood meal (column : Nb_fem_quete)(the values were randomly generated using the function =ALEA.ENTRE.BORNES(0, 12300) in Excel, in order to have plausible but not  field-based data.) and the mortality rate of mosquitoes (the values were randomly generated using the function =ALEA()*(0,01)+0,03) in Excel, in order to have plausible but not field-based data);
    - [mobilite_rum.csv](data/mobilite_rum.csv), a mock example of livestock mobility network in Senegal, used for testing the different functions, containing the name of the department of origin (Dept_or), the name of the department of destination (Dept_dest) and the number of displaced animals. The departments involved were chosen arbitrarily and values (column Nb) were randomly generated using the function =ALEA.ENTRE.BORNES(0, 400) in Excel, in order to have plausible but not  field-based data.);
    - [Nb_rum_mauritanie.xlsx](data/Nb_rum_mauritanie.xlsx), a mock example of an excel sheet containing the name of some departments in Mauritania and the number of animals presents in this department. (The values were randomly generated using the function =ALEA.ENTRE.BORNES(1000;12000000) in Excel, in order to have plausible but not  field-based data.);
    - [Nb_rum_senegal.xlsx](data/Nb_rum_senegal.xlsx), a mock example of an excel sheet containing the name of some departments in Senegal and the number of animals presents in this department (The values were randomly generated using the function =ALEA.ENTRE.BORNES(1000;12000000) in Excel, in order to have plausible but not  field-based data.) ;
 
  * **qGIS**  containing:
     - [qGIS/senegal](qGIS/senegal), A file containing all the shapefiles needed for the border of Senegal as well as the administrative division into 45 departments ;
    
  * **Scripts** containing several R scripts:
    - [usefull_functions.R](scripts/usefull_functions.R), a series of functions usefull to run the main script diffusion_disease.R ;
    - [6_nodes_network.R](scripts/6_nodes_network.R), A simplified model with only 6 nodes where different useful values (n: number of susceptible animals in a site or node, m: number of vector mosquitoes seeking a blood meal, p: daily probability of animals leaving a site, and R: the matrix of ruminant exchanges between sites) are all generated with predefined values ;
    - [barabasi_50_nodes_network.R](scripts/barabasi_50_nodes_network.R), the network is an undirected network with 50 nodes, constructed using the preferential attachment of the Barabási-Albert model. The susceptible animal population of nodes are identical (n=1000) while vector populations are randomly assigned in the range [300;1700];
    - [diffusion_disease.R](scripts/diffusion_disease.R), This is the main script with a situation close to a realistic scenario, using plausible data compared to those measured in the field.ll the data used in the script are fabricated (not extracted from real measured or estimated values) but are within the estimated possible ranges (see data).
  
  * **Outputs** containing one result from the script diffusion_disease.R: 
    - [ER_map.pdf](../output/ER_map.pdf), This is a map of the epidemic risk, normalized to the maximum value calculated, for susceptible animals present in the Senegalese departments. This is based on randomly generated data for the number n of susceptible animals per department, the number m of female mosquitoes seeking blood meals, the animal mobility (flow matrix R), and the daily probability of animals leaving the departments, denoted as p.;


### Description of the function: preparing_ruminants

The preparing_ruminants function is designed to prepare and process livestock data related to ruminants from multiple countries. It takes a list of file paths (Excel files) as input, where each file contains data about ruminants in a specific country. The function iterates through each country's data, extracts relevant columns (i.e., "departements" and "nb_total_rum"), rounds the "nb_total_rum" values to integers, and combines the data into a single data frame called "results." The data frame "results" contains the processed data from all countries.

#### Input

listpays: A vector containing file paths to Excel files, each representing data for the number of susceptible animals to the disease in a specific area of a country.

```
n<-preparing_ruminants(datapays)
```
#### Description

The function first creates an empty data frame named "results" to store the final processed data. Then, it iterates through each country's data, reads the Excel file, extracts the relevant columns ("departements" and "nb_total_rum"), rounds the "nb_total_rum" values to integers, and appends the country's data to the "results" data frame using the rbind function. After processing all the countries' data, the "results" data frame is sorted based on "departements" in ascending order, and the column names are renamed to "Departements" and "Nb_rum."

```
preparing_ruminants <- function(listpays) {
  results <- data.frame() 

  for (l in listpays) {
    Pays <- read_excel(l) 
    Nb_rum <- Pays[, c("departements", "nb_total_rum")] 
    Nb_rum$nb_total_rum <- round(Nb_rum$nb_total_rum, 0) 
    results <- rbind(results, Nb_rum) 
  }
  
  results <- arrange(results, departements) 
  colnames(results) <- c("Departements", "Nb_rum") 
  results$Nb_rum <- ifelse(results$Nb_rum == 0, 1, results$Nb_rum)
  return(results) # Return the final data frame with the prepared data.
}
```

Additionally, the function checks for "0" values in the "Nb_rum" column to avoid division by zero in matricial calculations. If a value is "0," it is replaced with 1 to ensure safe calculations. Finally, the processed data frame "results" is returned by the function.

This function is useful for consolidating and organizing data on ruminants from different countries into a single data frame, allowing for further analysis and processing of the livestock data.

#### Output

The output of this function is a data frame called "n" with two columns:

- Departements: Represents the departments (regions or administrative divisions) in each country where ruminants are located.

- Nb_rum: Represents the total number of ruminants in each department.

### Description of the function: get_mosquito_data

The get_mosquito_data function is designed to process and aggregate mosquito data related to the number of females in search of a blood meal and their mortality rate per department. It takes the path to an Excel file containing mosquito data as input. The function reads the Excel file and aggregates the data to calculate the total number of mosquitoes and the mean mortality rate per department. The results are combined into a data frame named "m" with meaningful column names.

#### Input

datamosquitoes: The file path to the Excel file containing mosquito data. The Excel file should contain columns for "departements" (representing the departments), "Nb_fem_quete" (representing the number of females in search of a blood meal), and "taux_mortalite" (representing the mortality rate of mosquitoes).

```
m <-get_mosquito_data(datamosquitoes)
```
#### Description

The function first reads the data on the number of females in search of a blood meal per department and their mortality rate from the Excel file using the read_excel function from the "readxl" package. It then aggregates the "Nb_fem_quete" column by "departements" to get the total number of mosquitoes per department using the aggregate function. Similarly, the function aggregates the "taux_mortalite" column by "departements" to get the mean mortality rate of mosquitoes per department.

```
 get_mosquito_data <- function(datamosquitoes) {
  nb_aedes <- read_excel(datamosquitoes)
  
  somme_aedes <- aggregate(Nb_fem_quete ~ departements, data = nb_aedes, FUN = sum)
  taux_aedes <- aggregate(taux_mortalite ~ departements, data = nb_aedes, FUN = mean)
  m <- merge(somme_aedes, taux_aedes, by = "departements", suffixes = c("_Nb_fem_quete", "_taux_mortalite"))
  colnames(m) <- c("Departements", "Nb_moustiques", "taux_mortalite_moyen")
  
  return(m)
}
```
Next, the function merges the two aggregated data frames based on the "departements" column to create a new data frame called "m" using the merge function. The column names of "m" are renamed to provide more meaningful names ("Departements", "Nb_moustiques" for the total number of mosquitoes, and "taux_mortalite_moyen" for the mean mortality rate).

#### Output

The output of this function is a data frame named "m" with three columns:

  - Departements: Represents the departments (regions or administrative divisions) where mosquitoes were observed.
  - Nb_moustiques: Represents the total number of mosquitoes in each department, calculated by aggregating the "Nb_fem_quete" column.
  - taux_mortalite_moyen: Represents the mean mortality rate of mosquitoes in each department, calculated by aggregating the "taux_mortalite" column.

### Description of the function: get_dept_mobil_data

The get_dept_mobil_data function is designed to generate a vector containing the names of departments implicated in animal mobility with other departments for which there is information about mosquitoes density. It takes two inputs: mobil, which is a data frame containing mobility data, and dept_ferlo, which is a list of departments belonging to the region who we have mosquitoes density (ferlo region in Senegal in this example).

#### Input

  - mobil: A data frame containing mobility data with columns "Dept_or" and "Dept_dest," representing the origin and destination departments, respectively.
  - dept_ferlo: A list of departments that belong to the "Ferlo" region with informations about mosquitoes density.

}
```
dept_mobil<-get_dept_mobil_data(mobil, dept_ferlo)
```
#### Description

The function first filters the mobility data (mobil) to include only rows where either the origin department ("Dept_or") or the destination department ("Dept_dest") is present in the "dept_ferlo" list. This ensures that only relevant departments involved in mobility with the "Ferlo" region are considered. The filtered data is stored in a new data frame called subset_dept.

Next, the function extracts unique values of "Dept_or" and "Dept_dest" from the filtered data (subset_dept). These unique values represent the set of all departments involved in the mobility analysis and are combined into a vector called dept_mobil.

```
get_dept_mobil_data <- function(mobil, dept_ferlo) {
  subset_dept <- subset(mobil, Dept_or %in% dept_ferlo | Dept_dest %in% dept_ferlo)
  dept_mobil <- unique(c(subset_dept$Dept_or, subset_dept$Dept_dest))
  dept_mobil <- unique(c(dept_mobil, dept_ferlo))
  dept_mobil <- sort(dept_mobil)
  return(dept_mobil)
}
```

The departments belonging to the "Ferlo" region (specified in dept_ferlo) are then added to the dept_mobil vector to ensure that all relevant departments are included. Duplicate entries in the vector are removed using the unique function. Finally, the dept_mobil vector is sorted in alphabetical order using the sort function to provide a consistent order of department names.

#### Output

The output of this function is a vector (dept_mobil) containing the names of departments involved in animal mobility with other departments for which there is information about mosquitoes density.

### Description of the function: generate_matrix_flux

The generate_matrix_flux function is designed to generate the matrix "matrice_flux," which represents the flux of animals between departments. It takes two inputs: mobil, a data frame containing mobility data, and dept_mobil, a vector containing the names of departments involved in animal mobility with other departments.

#### Input

  - mobil: A data frame containing mobility data with columns "Dept_or" and "Dept_dest," representing the origin and destination departments, respectively, and a column "Nb" representing the number of animals moving between the departments.
  - dept_mobil: A vector containing the names of departments involved in animal mobility with other departments for which we have information on mosquito densities.
```
matrice_flux<-generate_matrix_flux(mobil, dept_mobil)
```
#### Description

The function first calculates the total mobility (sum of "Nb") between each pair of origin and destination departments and stores the result in the data frame "echanges_flux." It uses the dplyr package to group the data by "Dept_or" and "Dept_dest" and then applies the summarise function to calculate the sum of "Nb" for each group.

Next, the function normalizes the total mobility values by each origin department, converting them into proportions, and updates the "total" column in the "echanges_flux" data frame. It groups the data by "Dept_or" again and uses the mutate function to calculate the proportion of total mobility for each origin department.The columns of the "echanges_flux" data frame are renamed to provide more descriptive names ("Dept_Or," "Dept_Dest," and "total").

Next, the function creates an empty matrix "matrice_flux" with dimensions based on the number of unique departments in "dept_mobil." The matrix will be square, with rows and columns corresponding to the departments in "dept_mobil." The dimnames argument is used to assign the names of departments to the rows and columns of the matrix.

```
generate_matrix_flux <- function(mobil, dept_mobil) {
  echanges_flux <- mobil %>%
    group_by(Dept_or, Dept_dest) %>%
    summarise(total = sum(Nb))
  
  echanges_flux <- echanges_flux %>%
    group_by(Dept_or) %>%
    mutate(total = prop.table(total))
  
  colnames(echanges_flux) <- c("Dept_Or", "Dept_Dest", "total")
  
  matrice_flux <- matrix(0, nrow = length(dept_mobil), ncol = length(dept_mobil),   dimnames = list(dept_mobil, dept_mobil))
  
  for (i in 1:nrow(echanges_flux)) {
    dept_or <- echanges_flux$Dept_Or[i]
    dept_dest <- echanges_flux$Dept_Dest[i]
    value <- echanges_flux$total[i]
    
    if (dept_or %in% dept_mobil && dept_dest %in% dept_mobil) {
      matrice_flux[dept_or, dept_dest] <- value
    }
  }
  
  return(matrice_flux)
}

```
The function then fills in the "matrice_flux" matrix with the normalized mobility values. It iterates through each row of the "echanges_flux" data frame and extracts the origin department ("Dept_or"), destination department ("Dept_Dest"), and the normalized mobility value ("total"). If both the origin and destination departments are present in "dept_mobil," the function assigns the "total" value to the corresponding cell in "matrice_flux."

#### Output

The output of this function is a matrix named "matrice_flux" with rows and columns corresponding to the names of departments involved in animal mobility with other departments. The elements of the matrix represent the normalized mobility values, indicating the proportion of animal mobility between the departments.

### Description of the function: generate_prob_sortie

The generate_prob_sortie function is designed to compute the probability (p) of ruminants leaving each origin department ("Dept_or"). It takes two inputs: mobil, a data frame containing mobility data, and n, a data frame containing information about the total number of ruminants in each department.

#### Input

  - mobil: A data frame containing mobility data with columns "Dept_or" and "Dept_dest," representing the origin and destination departments, respectively, and a column "Nb" representing the number of animals moving between the departments.
  - n: A data frame containing information about the total number of ruminants in each department, with columns "Departements" (representing the departments) and "Nb_rum" (representing the total number of ruminants).
```
prob_sortie <-generate_prob_sortie(mobil, n)
```
#### Description

The function first calculates the total number of movements (sum of "Nb") from each origin department ("Dept_or") and stores the result in the data frame "prob_sortie." It uses the dplyr package to group the data by "Dept_or" and then applies the summarise function to calculate the sum of "Nb" for each group.Next, the columns of the "prob_sortie" data frame are renamed to provide more descriptive names ("Dept_Or" and "Total"). The function then adds a new column "nb_total_rum_or" to the "prob_sortie" data frame, representing the total number of ruminants ("Nb_rum") for each origin department ("Dept_or"). The data is obtained from the "n" data frame using the match function, which finds the corresponding "Nb_rum" value for each department in "prob_sortie."Next, the function calculates the probability of ruminants leaving each origin department ("Dept_or") for a blood meal. The probability "p" is computed based on the ratio of the total movement ("Total") to the total number of ruminants in the origin department ("nb_total_rum_or"). If the total movement ("Total") is less than or equal to the total number of ruminants ("nb_total_rum_or"), the function divides "Total" by "nb_total_rum_or." Otherwise, it divides "Total" by the sum of "nb_total_rum_or" and "Total." The function then divides the probability "p" by 30 to obtain the daily number of displaced animals. Finally, the function sets the probability "p" to 1 if it exceeds 1 (to avoid values greater than 1) and returns the "prob_sortie" data frame containing the calculated probabilities.
```
generate_prob_sortie <- function(mobil, n) {
  prob_sortie <- mobil %>%
    group_by(Dept_or) %>%
    summarise(Total = sum(Nb))
  
  colnames(prob_sortie) <- c("Dept_Or", "Total")
  
  prob_sortie <- prob_sortie %>%
    mutate(nb_total_rum_or = n$Nb_rum[match(Dept_Or, n$Departements)])
  
  prob_sortie$p <- ifelse(prob_sortie$Total <= prob_sortie$nb_total_rum_or, prob_sortie$Total / prob_sortie$nb_total_rum_or, prob_sortie$Total / (prob_sortie$nb_total_rum_or + prob_sortie$Total))
  
  prob_sortie$p <- 1/30 * prob_sortie$p
  
  prob_sortie$p <- ifelse(prob_sortie$p > 1, 1, prob_sortie$p)
  
  return(prob_sortie)
}
```

#### Output

The output of this function is a data frame named "prob_sortie" with three columns:

  -Dept_Or: Represents the origin departments (regions or administrative divisions).
  - Total: Represents the total number of animals displaced (sum of "Nb") from each origin department.
  - p: Represents the probability of ruminants leaving each origin department. The probability is calculated based on the ratio of the total movement to the total number of ruminants in the origin department, with a maximum value of 1. The values are divided by 30 to obtain the daily number of displaced animals.
  
### Description of the function: generate_df_global

The generate_df_global function is designed to generate a data frame containing all the information needed to compute the "M" and "Mtilde" matrices. It takes four inputs: dept_mobil, which is a vector containing the names of departments implicated in animal mobility with other departments for which there is information about mosquitoes density, n, which is a data frame containing information about the total number of ruminants in each department, m, which is a data frame containing mosquito data including the total number of mosquitoes and the mean mortality rate per department, and prob_sortie, which is a data frame containing the probability of ruminants leaving each origin department for a blood meal.

#### Input

  - dept_mobil: A vector containing the names of departments involved in animal mobility with other departments for which there is information about mosquitoes density.
  - n: A data frame containing information about the total number of ruminants in each department, with columns "Departements" (representing the departments) and "Nb_rum" (representing the total number of ruminants).
  - m: A data frame containing mosquito data with columns "Departements" (representing the departments), "Nb_moustiques" (representing the total number of mosquitoes), and "taux_mortalite_moyen" (representing the mean mortality rate of mosquitoes).
  - prob_sortie: A data frame containing the probability of ruminants leaving each origin department for a blood meal, with columns "Dept_Or" (representing the origin departments) and "p" (representing the probability).
```
df_global <-generate_df_global(dept_mobil, n, m, prob_sortie)
```

#### Description

The function first creates a data frame "df_global" with a single column "Departements" containing the names of departments from the "dept_mobil" vector. Next, the function merges the "df_global" data frame with the "n" data frame based on the "Departements" column, keeping all rows from "df_global." This adds the columns "Ruminants" (total number of ruminants) to "df_global."Similarly, the function merges the "df_global" data frame with the "m" data frame based on the "Departements" column, keeping all rows from "df_global." This adds the columns "Moustiques" (total number of mosquitoes) and "Mortalite" (mean mortality rate of mosquitoes) to "df_global."Any NA values in the "Nb_moustiques" and "taux_mortalite_moyen" columns of "df_global" are replaced with 0 using the ifelse function.Next, the function merges the "df_global" data frame with the "prob_sortie" data frame based on the "Departements" and "Dept_Or" columns, keeping all rows from "df_global." This adds the column "p" (probability of ruminants leaving each origin department for a blood meal) to "df_global." Any NA values in the "p" column of "df_global" are replaced with 0 using the ifelse function. Finally, the columns of "df_global" are renamed to have more descriptive names ("Departements," "Ruminants," "Moustiques," "Mortalite," and "p").The function returns the "df_global" data frame containing all the necessary information to compute the "M" and "Mtilde" matrices.

```
generate_df_global <- function(dept_mobil, n, m, prob_sortie) {
  df_global <- data.frame(Departements = dept_mobil)
  df_global <- merge(df_global, n, by = "Departements", all.x = TRUE)
  df_global <- merge(df_global, m, by = "Departements", all.x = TRUE)
  df_global$Nb_moustiques <- ifelse(is.na(df_global$Nb_moustiques), 0, df_global$Nb_moustiques)
  df_global$taux_mortalite_moyen <- ifelse(is.na(df_global$taux_mortalite_moyen), 0, df_global$taux_mortalite_moyen)
  df_global <- merge(df_global, prob_sortie[c("Dept_Or", "p")], by.x = "Departements", by.y = "Dept_Or", all.x = TRUE)
  df_global$p <- ifelse(is.na(df_global$p), 0, df_global$p)
  names(df_global) <- c("Departements", "Ruminants", "Moustiques", "Mortalite", "p")
  return(df_global)
}
```
#### Output

The output of this function is a data frame named "df_global" with five columns:

  - Departements: Represents the departments (regions or administrative divisions) involved in animal mobility with other departments for which there is information about mosquitoes density.
  - Ruminants: Represents the total number of ruminants in each department, obtained from the "n" data frame.
  - Moustiques: Represents the total number of mosquitoes in each department, obtained from the "m" data frame.
  - Mortalite: Represents the mean mortality rate of mosquitoes in each department, obtained from the "m" data frame.
  - p: Represents the probability of ruminants leaving each origin department for a blood meal, obtained from the "prob_sortie" data frame.
  
  
### Description of the function: create_matrix_M

The create_matrix_M function is designed to generate the "M" matrix based on the input data. It takes four inputs: m, which is a data frame containing mosquito data, n, which is a data frame containing information about the total number of ruminants in each department, R, which is a matrix representing the movement of animals between departments, and p, which is a vector representing the probability of ruminants leaving each origin department for a blood meal.

#### Input

  - m: A data frame containing mosquito data with columns "Departements" (representing the departments), "Nb_moustiques" (representing the total number of mosquitoes), and "taux_mortalite_moyen" (representing the mean mortality rate of mosquitoes).
  - n: A data frame containing information about the total number of ruminants in each department, with columns "Departements" (representing the departments) and "Nb_rum" (representing the total number of ruminants).
  - R: A matrix representing the movement of animals between departments, with rows and columns named after the departments and values representing the movement from one department to another.
  - p: A vector representing the probability of ruminants leaving each origin department for a blood meal.

```  
M <-create_matrix_M(m, n, R, p)

```

#### Description

The function first calculates the complement of the probability vector "p" and stores it in "q."

Next, the function creates two diagonal matrices "matq" and "matp" using the "q" and "p" vectors, respectively. The dimensions and names of the matrices are set to correspond to the names of the departments.

The effective number of animals in each department ("n_eff") is then calculated using the matrices "matq," "matp," and the "n" data frame. The effective number is obtained by applying the formulas: "n_eff = matq * n + matp * t(R) * n."

The inverse of the effective number ("inv_n_eff") is calculated element-wise, and the diagonal matrix is created using the "inv_n_eff" vector. The dimensions and names of the matrix are set to correspond to the names of the departments.

Next, the matrix "m_tilde" is obtained by element-wise multiplication of "inv_n_eff" and "m," which represents the effective number of mosquitoes in each department.

The local mosquito movement matrix ("M_loc") is computed by applying the formula: "M_loc = matq * m_tilde."

The movement of mosquitoes between departments ("M_mouvement") is calculated by first computing "M_2 = R * matp" and then applying the formula: "M_mouvement = M_2 * m_tilde."

Finally, the "M" matrix is obtained by adding "M_loc" and "M_mouvement."

The function returns the "M" matrix.


```
create_matrix_M <- function(m, n, R, p) {
  q <- 1 - p
  matq <- diag(q)
  dimnames(matq) <- list(names(q), names(q))
  matp <- diag(p)
  dimnames(matp) <- list(names(p), names(p))

  n_eff <- matq %*% n + matp %*% t(R) %*% n
  inv_n_eff <- diag(sapply(n_eff, function(x) 1/x))
  dimnames(inv_n_eff) <- list(names(p), names(p))
  m_tilde <- inv_n_eff * m

  M_loc <- matq %*% m_tilde
  M_2 <- R %*% matp
  M_mouvement <- M_2 %*% m_tilde

  M <- M_loc + M_mouvement
  return(M)
}
```

#### Output

The output of this function is a matrix "M". The rows and columns of the matrix are named after the departments.

### Description of the function: create_matrix_Mtilde

The create_matrix_Mtilde function is designed to generate the "Mtilde" matrix based on the input data. It takes four inputs: m, which is a data frame containing mosquito data, n, which is a data frame containing information about the total number of ruminants in each department, R, which is a matrix representing the movement of animals between departments, and p, which is a vector representing the probability of ruminants leaving each origin department for a blood meal.

#### Input

  -m: A data frame containing mosquito data with columns "Departements" (representing the departments), "Nb_moustiques" (representing the total number of mosquitoes), and "taux_mortalite_moyen" (representing the mean mortality rate of mosquitoes).
  -n: A data frame containing information about the total number of ruminants in each department, with columns "Departements" (representing the departments) and "Nb_rum" (representing the total number of ruminants).
  -R: A matrix representing the movement of animals between departments, with rows and columns named after the departments and values representing the movement from one department to another.
  -p: A vector representing the probability of ruminants leaving each origin department for a blood meal.
```
Mtilde <- create_matrix_Mtilde(m, n, R, p)
```
#### Description

The function first calculates the complement of the probability vector p and stores it in q.

Next, the function creates two diagonal matrices matq and matp using the q and p vectors, respectively. The dimensions and names of the matrices are set to correspond to the names of the departments.

The effective number of animals in each department (n_eff) is then calculated using the matrices matq, matp, and the n data frame. The effective number is obtained by applying the formulas: n_eff = matq * n + matp * t(R) * n.

The inverse of the effective number (inv_n_eff) is calculated element-wise, and the diagonal matrix is created using the inv_n_eff vector. The dimensions and names of the matrix are set to correspond to the names of the departments.

Next, the matrix m_tilde is obtained by element-wise multiplication of inv_n_eff and m, which represents the effective number of mosquitoes in each department.

The local mosquito movement matrix (M_loc) is computed by applying the formula: M_loc = matq * m_tilde.

The movement of mosquitoes between departments (M_mouvement) is calculated by first computing M_2 = t(R) %*% matp and then applying the formula: M_mouvement = inv_n_eff %*% M_2 %*% diag(n).

Finally, the Mtilde matrix is obtained by adding M_loc and M_mouvement.

```
create_matrix_Mtilde <- function(m, n, R, p) {
  q <- 1 - p
  matq <- diag(q)
  dimnames(matq) <- list(names(q), names(q))
  matp <- diag(p)
  dimnames(matp) <- list(names(p), names(p))

  n_eff <- matq %*% n + matp %*% t(R) %*% n
  inv_n_eff <- diag(sapply(n_eff, function(x) 1/x))
  inv_n_eff <- ifelse(is.infinite(inv_n_eff), 1, inv_n_eff)
  dimnames(inv_n_eff) <- list(names(p), names(p))
  m_tilde <- inv_n_eff * m

  M_loc <- matq %*% m_tilde
  M_2 <- t(R) %*% matp
  dg <- diag(n)
  M_mouvement <- inv_n_eff %*% M_2 %*% dg

  Mtilde <- M_loc + M_mouvement
  return(Mtilde)
}
```

#### Output

The output of this function is a matrix Mtilde. The rows and columns of the matrix are named after the departments.

### Description of the function: calculate_values

The calculate_values function is designed to calculate the values of the metapopulation model based on the provided input parameters. It takes six inputs: beta and lambda, which are epidemiological variables, df_global, which is a data frame containing global information about departments, R_i, which is a matrix representing the movement of ruminants between departments, M and Mt, which are matrices representing the effective movement of mosquitoes between departments.

#### Input

  - beta: A parameter representing the  rate of mosquito female bites in search of a blood meal on susceptible animals(biting rate) (epidemiological variable).
  - lambda: A parameter representing the probability of infection with the mosquitoe bite (he healthy mosquito becomes infected after biting an infected animal, or the healthy animal becomes infected after being bitten by an infected mosquito)(epidemiological variable).
  - df_global: A data frame containing information about departments with columns "Departements" (representing the departments), "Ruminants" (representing the total number of ruminants), "Moustiques" (representing the total number of mosquitoes), "Mortalite" (representing the mortality rate of mosquitoes), and "p" (representing the probability of ruminants leaving each origin department for a blood meal).
  - R_i: A matrix representing the movement of ruminants between departments, with rows and columns named after the departments and values representing the movement from one department to another.
  - M: A matrix representing the infection from susceptibles animals (here ruminants) to mosquitoes.
  - Mt: A matrix representing the infection from mosquitoes to susceptibles animals( here ruminants).

```  
valeurs<-calculate_values(beta, lambda, df_global, R_i, M, Mt)
```

#### Description

The function first assigns the values of the epidemiological variables "beta" and "lambda" to the variables "b" and "l," respectively. Next, the mortality rate of mosquitoes ("mu_moustique") is extracted from the "Mortalite" column of the "df_global" data frame. The inverse of the mortality rate of mosquitoes ("inv_mu_moustique") is calculated element-wise. A diagonal matrix "diag_inv_mu_M" is created using the "inv_mu_moustique" vector as diagonal elements. Any infinite values in the "diag_inv_mu_M" matrix are replaced with 1 to avoid issues in further calculations.
The value 6 is assigned to the variable "inv_mu_ruminants." A diagonal matrix "diag_inv_mu_H" is created using "inv_mu_ruminants" as diagonal elements. The number of rows in "diag_inv_mu_H" is determined by the number of rows in "R_i." Finally, the matrix "valeurs" is calculated based on the metapopulation model using the input parameters "b," "l," "diag_inv_mu_H," "diag_inv_mu_M," "M," and "Mt."


```
calculate_values <- function(beta, lambda, df_global, R_i, M, Mt) {
  b <- beta
  l <- lambda
  mu_moustique <- df_global$Mortalite
  inv_mu_moustique <- 1 / mu_moustique
  diag_inv_mu_M <- diag(inv_mu_moustique)
  diag_inv_mu_M <- replace(diag_inv_mu_M, is.infinite(diag_inv_mu_M), 1)
  inv_mu_ruminants <- 6
  diag_inv_mu_H <- diag(rep(inv_mu_ruminants, nrow(R_i)))
  valeurs <- b^2 * l^2 * diag_inv_mu_H %*% diag_inv_mu_M %*% M %*% Mt
  return(valeurs)
}
```
#### Output

The function returns the matrix "valeurs," which represents the calculated values of the metapopulation model.



### Description of the function: calculate_ER

The calculate_ER function is designed to calculate the epidemic risk (ER) for each department based on the provided input parameters. It takes three inputs: M and Mt, which are matrices representing the processus of contagion between mosquitoes and susceptibles animalsinside a site and through animal mobility and n_i, which represents the number of ruminants in a given department.

#### Input

  - M: M: A matrix representing the infection from susceptibles animals (here ruminants) to mosquitoes.
  - Mt: A matrix representing the infection from mosquitoes to susceptibles animals(here ruminants).
  - n_i: A data fram with the number of ruminants in some departments.

``` 
ER<-calculate_ER(M, Mt, n_i)
```
#### Description

The function first performs matrix multiplication of "M" with "Mt" and stores the result in "H." Next, the row sums of "H" are calculated, representing the total effective contagion between ruminants through vectorial transmission. The result is stored in "ER." Each element of "ER" is then multiplied by "n_i," which relates to the number of ruminants in the corresponding department. The elements of "ER" are rounded to zero decimal places using the round function.

```
calculate_ER <- function(M, Mt, n_i) {
  H <- M %*% Mt
  ER <- rowSums(H) * n_i
  ER <- round(ER, digits = 0)
  return(ER)
}
```
#### Output 

The function returns the "ER" vector, representing the calculated epidemic risk for each department. The higher the value of ER, the higher the risk of disease spread from that department to other departments in the metapopulation model


## Authors and acknowledgment

Codes have been developed by [Yannick HUYET University of Montpellier](mailto:yannick.huyet@gmail.com), under the supervision of [Andrea Apolloni-CIRAD](mailto:andrea.apolloni@cirad.fr), [Mamadou Ciss-ISRA](mailto:ciss.mamadou@gmail.com), [Luca Ciandrini ](mailto:luca.ciandrini@umontpellier.fr), [Assane Gueye Fall ISRA](mailto:agueyefall@yahoo.fr).

This work has been supported by the Projects Kim RIVE and RIVOC. 

## Citation


## License



 
